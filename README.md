# 24hducode2023

## Parties 1 & 3
Entrainements de modèles CoreML (avec CreateML d'Apple).
Création d'une application minimale iOS pour exploiter les modèles.
Début de l'idée d'un scénario utilisateur pour rendre pratiques les usages (approche orientée utilisateurs).

Intégration des solutions et test UI/UX sur le smartphone.
Itérations pour arriver à un résultat acceptable pour l'utilisateur.

Démarrage de la réflexion autour de la réalisation du book de la partie 5.

## Partie 2
Utilisation des modèles Huggingface et réalisation d'un serveur en Python / hosting du serveur sur un VPS : premier KO (capacités de traitement réduites, temps d'exécution anormalement long). 4h00/5h00 du matin !

Second KO : mise à jour du MacOS de réalisation pour faire tourner le serveur (dépendances logicielles à l'architecture matérielle du processeur). 8h00 du matin !

Solution pour la démo : utiliser les modèles en mode "bouchonnés"sur la cible.

## Partie 4
L'intégration de la solution ayant démarré tôt dans le processus de développement, l'intégration a été continue et itérative tout au long de la journée.

## Partie 5
Réflexion autour du story telling des modes de fonctionnement itératifs et mise en avant de la stratégie orientée utilisateurs.
