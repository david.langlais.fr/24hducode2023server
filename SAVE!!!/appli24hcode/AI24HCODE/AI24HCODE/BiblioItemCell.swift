//
//  BiblioItemCell.swift
//  AI24HCODE
//
//  Created by David DRISSI on 01/04/2023.
//

import Foundation
import UIKit

class BiblioItemCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var imageProba: UIImageView!
}
