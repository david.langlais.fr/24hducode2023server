//
//  BibliothequeViewController.swift
//  AI24HCODE
//
//  Created by David DRISSI on 01/04/2023.
//

import Foundation
import UIKit
import PhotosUI
import VideoToolbox

class BibliothequeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet var importerBouton: UIBarButtonItem!
    @IBOutlet var biblioCollectionView: UICollectionView!
    
    private var libraryPicker: UIImagePickerController?
    private var imageCapturee: UIImage?
    
    static var selectedImageView: UIImageView?
    static var selectedCategory: ClassificationModelPrediction?
    
    static var imagesOk = [Item]()
    static var imagesRetouches = [Item]()
    static var imagesInvalid = [Item]()
    
    
    
    struct Item {
        let image: UIImage
        let imageProba: UIImage?
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biblioCollectionView.reloadData()
    }
    
    func requestPhotoPermission() {
        let showLibrary = {
            /*PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: self) { resultats in
                print(resultats)
                
                
                let existingSelection = self.selection
                var newSelection = [String: PHPickerResult]()
                for result in resultats {
                    let identifier = result
                    newSelection[identifier] = existingSelection[identifier] ?? result
                }

                // Track the selection in case the user deselects it later.
                selection = newSelection
            }*/
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let requestPermission = {
            PHPhotoLibrary.requestAuthorization(for: .readWrite) { [unowned self] (status) in
                DispatchQueue.main.async { [unowned self] in
                    guard status == .authorized || status == .limited else {
                        let alert = UIAlertController(title: "Permission d'accès aux photos refusée", message: "Rendez-vous dans les paramètres pour accepter cette permission", preferredStyle: .alert)
                        alert.addActionGoToPrivacySettings()
                        self.present(alert, animated: true)
                        return
                    }
                    
                    if status == .limited {
                        showLibrary()
                    }
                }
            }
        }
        
        let currentStatus = PHPhotoLibrary.authorizationStatus(for: .readWrite)
        
        switch currentStatus {
        case .notDetermined:
            requestPermission()
        case .restricted:
            let alert = UIAlertController(title: "Accès aux photos restreint", message: "L'accès aux photos n'est pas possible sur votre appareil", preferredStyle: .alert)
            self.present(alert, animated: true)
        case .denied:
            let alert = UIAlertController(title: "Permission d'accès aux photos refusée", message: "Rendez-vous dans les paramètres pour accepter cette permission", preferredStyle: .alert)
            alert.addActionGoToPrivacySettings()
            self.present(alert, animated: true)
        case .authorized:
            let alert = UIAlertController(title: "Accès à toutes les photos donnée", message: "Vous avez donné accès à toutes les photos de votre bibliothèque d'images", preferredStyle: .alert)
            self.present(alert, animated: true)
        case .limited:
            showLibrary()
        @unknown default:
            break
        }
    }
    
    func requestCameraPermission() {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        
        let afficherErreurPermission = {
            let alert = UIAlertController(title: "Permission caméra refusée", message: "Rendez-vous dans les paramètres pour accepter la permission caméra", preferredStyle: .alert)
            alert.addActionGoToPrivacySettings()
            self.present(alert, animated: true)
        }
        
        switch status {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video,completionHandler: { result in
                if result == false {
                    afficherErreurPermission()
                    return
                }
                
                self.takePhoto()
            })
        case .restricted:
            let alert = UIAlertController(title: "Prise de photo restreinte", message: "La prise de photo n'est pas possible sur votre appareil", preferredStyle: .alert)
            self.present(alert, animated: true)
        case .denied:
            afficherErreurPermission()
        case .authorized:
            takePhoto()
        }
    }
    
    func takePhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func clicImportBouton() {
        
        let cameraButton = UIAlertAction(title: "Prendre une photo", style: .default) { action in
            self.requestCameraPermission()
        }
        
        let libraryButton = UIAlertAction(title: "Sélectionner dans Photos", style: .default) { action in
            self.requestPhotoPermission()
        }
        
        let cancelButton = UIAlertAction(title: "Annuler", style: .cancel)
        
        
        
        let alert = UIAlertController(title: "Sélectionner une option pour importer une photo", message: nil, preferredStyle: .alert)
        alert.addAction(cameraButton)
        alert.addAction(libraryButton)
        alert.addAction(cancelButton)
        self.present(alert, animated: true)
        
        
    }
    
    @objc func longPressOnImage(_ sender: UILongPressGestureRecognizer) {
        guard BibliothequeViewController.selectedImageView == nil else {
            return
        }
        
        if let cellView = sender.view {
            
            
            
            BibliothequeViewController.selectedImageView = cellView.subviews(ofType: UIImageView.self).first
            
            
            let imageOkContains = BibliothequeViewController.imagesOk.contains { item in
                item.image == BibliothequeViewController.selectedImageView?.image
            }
            
            let imageRetouchesContains = BibliothequeViewController.imagesRetouches.contains { item in
                item.image == BibliothequeViewController.selectedImageView?.image
            }
            
            let imageInvalidContains = BibliothequeViewController.imagesInvalid.contains { item in
                item.image == BibliothequeViewController.selectedImageView?.image
            }
            
            if imageOkContains {
                BibliothequeViewController.selectedCategory = .ok
            }
            else if imageRetouchesContains {
                BibliothequeViewController.selectedCategory = .retoucher
            }
            else {
                BibliothequeViewController.selectedCategory = .exclure
            }
            
            self.performSegue(withIdentifier: "toBiblioDetails", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsVC = segue.destination as? BiblioDetailsViewController {
            detailsVC.biblioVC = self
        }
    }
    
    
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imageCapturee = info[.originalImage] as? UIImage else {
            let alert = UIAlertController(title: "Erreur lors de la sauvegarde de la photo", message: nil, preferredStyle: .alert)
            self.present(alert, animated: true)
            return
        }
        
        self.libraryPicker = picker
        
        DispatchQueue.main.async {
            UIImageWriteToSavedPhotosAlbum(imageCapturee, self, #selector(self.sauvegardePhotoTerminee), nil)
        }
        
    }*/
    
    func sauvegarder(image: UIImage) {
        DispatchQueue.main.async {
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.sauvegardePhotoTerminee), nil)
        }
    }
    
    @objc func sauvegardePhotoTerminee() {
        self.libraryPicker?.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage,
           let ciImage = CIImage(image: image) {
            
            let context = CIContext(options: nil)
            if let cgImage = context.createCGImage(ciImage, from: ciImage.extent){
                
                let input = try! classification_imagesInput(imageWith: cgImage)
                let model = classification_images()
                let output = try! model.prediction(input: input)
                
                
                
                
                let prediction = ClassificationModelPrediction(rawValue: output.classLabel)
                
                var imageProba : UIImage?
                if let probability = output.classLabelProbs[prediction!.rawValue] {
                    
                    if probability > 0.95 {
                        imageProba = UIImage(named: "vert")
                    }
                    else if probability > 0.8 {
                        imageProba = UIImage(named: "orange")
                    }
                    else {
                        imageProba = UIImage(named: "rouge")
                    }
                }
                
                switch prediction {
                case .ok:
                    
                    self.applyStyle1(image: image)
                    self.applyStyle2(image: image)
                    self.applyStyle3(image: image)
                    self.applyStyle4(image: image)
                    
                    BibliothequeViewController.imagesOk.append(Item(image: image, imageProba: imageProba))
                case .retoucher: BibliothequeViewController.imagesRetouches.append(Item(image: image, imageProba: imageProba))
                case .exclure: BibliothequeViewController.imagesInvalid.append(Item(image: image, imageProba: imageProba))
                default: break
                }
                
                self.biblioCollectionView.reloadData()
            }
        }
        
        picker.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    func applyStyle1(image: UIImage) {
        if let ciImage = CIImage(image: image) {
            
            let context = CIContext(options: nil)
            if let cgImage = context.createCGImage(ciImage, from: ciImage.extent){
                let transformModel = style_vangogh()
                let transformOutput = try! transformModel.prediction(input: style_vangoghInput(imageWith: cgImage))
                if let imageStylisee = UIImage(pixelBuffer: transformOutput.stylizedImage) {
                    BookListeViewController.imagesStyle1.append(imageStylisee)
                }
            }
        }
    }
    
    func applyStyle2(image: UIImage) {
        if let ciImage = CIImage(image: image) {
            
            let context = CIContext(options: nil)
            if let cgImage = context.createCGImage(ciImage, from: ciImage.extent){
                let transformModel = style_ad()
                let transformOutput = try! transformModel.prediction(input: style_adInput(imageWith: cgImage))
                if let imageStylisee = UIImage(pixelBuffer: transformOutput.stylizedImage) {
                    BookListeViewController.imagesStyle2.append(imageStylisee)
                }
            }
        }
    }
    
    func applyStyle3(image: UIImage) {
        if let ciImage = CIImage(image: image) {
            
            let context = CIContext(options: nil)
            if let cgImage = context.createCGImage(ciImage, from: ciImage.extent){
                let transformModel = style_comic()
                let transformOutput = try! transformModel.prediction(input: style_comicInput(imageWith: cgImage))
                if let imageStylisee = UIImage(pixelBuffer: transformOutput.stylizedImage) {
                    BookListeViewController.imagesStyle3.append(imageStylisee)
                }
            }
        }
    }
    func applyStyle4(image: UIImage) {
        if let ciImage = CIImage(image: image) {
            
            let context = CIContext(options: nil)
            if let cgImage = context.createCGImage(ciImage, from: ciImage.extent){
                let transformModel = style_punk()
                let transformOutput = try! transformModel.prediction(input: style_punkInput(imageWith: cgImage))
                if let imageStylisee = UIImage(pixelBuffer: transformOutput.stylizedImage) {
                    BookListeViewController.imagesStyle4.append(imageStylisee)
                }
            }
        }
    }
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0: return BibliothequeViewController.imagesOk.count
        case 1: return BibliothequeViewController.imagesRetouches.count
        case 2: return BibliothequeViewController.imagesInvalid.count
        default: return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "biblioItemCell", for: indexPath) as! BiblioItemCell
    
        cell.backgroundColor = .red
        
        switch indexPath.section {
        case 0:
            cell.image.image = BibliothequeViewController.imagesOk[indexPath.row].image
            cell.imageProba.image = BibliothequeViewController.imagesOk[indexPath.row].imageProba
        case 1:
            cell.image.image = BibliothequeViewController.imagesRetouches[indexPath.row].image
            cell.imageProba.image = BibliothequeViewController.imagesRetouches[indexPath.row].imageProba
        case 2:
            cell.image.image = BibliothequeViewController.imagesInvalid[indexPath.row].image
            cell.imageProba.image = BibliothequeViewController.imagesInvalid[indexPath.row].imageProba
        default: break
        }
        
        if cell.gestureRecognizers == nil || cell.gestureRecognizers?.isEmpty == true {
            let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressOnImage(_:)))
            longPressGesture.minimumPressDuration = 0.5
            cell.isUserInteractionEnabled = true
            cell.addGestureRecognizer(longPressGesture)
        }
        
        return cell
    }
    
    static func changeCategory(image: UIImage, newCategory: ClassificationModelPrediction) {
        var proba : UIImage?
        
        BibliothequeViewController.imagesOk.removeAll { item in
            if item.image == image {
                proba = item.imageProba
            }
            
            return item.image == image
        }
        BibliothequeViewController.imagesRetouches.removeAll { item in
            if item.image == image {
                proba = item.imageProba
            }
            
            return item.image == image
        }
        BibliothequeViewController.imagesInvalid.removeAll { item in
            if item.image == image {
                proba = item.imageProba
            }
            
            return item.image == image
        }
        
        switch newCategory {
        case .exclure:
            BibliothequeViewController.imagesInvalid.append(Item(image: image, imageProba: proba))
        case .ok:
            BibliothequeViewController.imagesOk.append(Item(image: image, imageProba: proba))
        case .retoucher:
            BibliothequeViewController.imagesRetouches.append(Item(image: image, imageProba: proba))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                layout collectionViewLayout: UICollectionViewLayout,
                                sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = self.biblioCollectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "biblioHeaderView", for: indexPath) as! BiblioHeaderView
            
            switch indexPath.section {
            case 0: headerView.titre.text = "Valide"
            case 1: headerView.titre.text = "À retoucher"
            case 2: headerView.titre.text = "Invalide (image exclue)"
            default: headerView.titre.text = ""
            }
            
            return headerView
        default: break
        }
        
        return UICollectionReusableView(frame: CGRect.zero)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }

    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 100, height: 40)
    }
    
}

extension UIAlertController {
    func addActionGoToPrivacySettings() {
        let action = UIAlertAction(title: "Modifier les paramètres", style: .default) { action in
            guard let url = URL(string: UIApplication.openSettingsURLString),
                UIApplication.shared.canOpenURL(url) else {
                    return
            }

            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        
        self.addAction(action)
    }
}

extension UIImage {
    public convenience init?(pixelBuffer: CVPixelBuffer) {
        var cgImage: CGImage?
        VTCreateCGImageFromCVPixelBuffer(pixelBuffer, options: nil, imageOut: &cgImage)

        guard let cgImage = cgImage else {
            return nil
        }

        self.init(cgImage: cgImage)
    }
}

extension UIView {
    func subviews<T:UIView>(ofType WhatType:T.Type) -> [T] {
        var result = self.subviews.compactMap {$0 as? T}
        for sub in self.subviews {
            result.append(contentsOf: sub.subviews(ofType:WhatType))
        }
        return result
    }
}

enum ClassificationModelPrediction: String {
    case exclure
    case ok
    case retoucher
}
