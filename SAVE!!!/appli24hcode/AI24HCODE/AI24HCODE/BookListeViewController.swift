//
//  BookListeViewController.swift
//  AI24HCODE
//
//  Created by David DRISSI on 01/04/2023.
//

import Foundation
import UIKit

class BookListeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var shareBouton: UIBarButtonItem!
    @IBOutlet var biblioCollectionView: UICollectionView!
    
    static var imagesStyle1 = [UIImage]()
    static var imagesStyle2 = [UIImage]()
    static var imagesStyle3 = [UIImage]()
    static var imagesStyle4 = [UIImage]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.biblioCollectionView.reloadData()
    }
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0: return BookListeViewController.imagesStyle1.count
        case 1: return BookListeViewController.imagesStyle2.count
        case 2: return BookListeViewController.imagesStyle3.count
        case 3: return BookListeViewController.imagesStyle4.count
        default: return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "biblioItemCell", for: indexPath) as! BiblioItemCell
    
        cell.backgroundColor = .red
        
        switch indexPath.section {
        case 0: cell.image.image = BookListeViewController.imagesStyle1[indexPath.row]
        case 1: cell.image.image = BookListeViewController.imagesStyle2[indexPath.row]
        case 2: cell.image.image = BookListeViewController.imagesStyle3[indexPath.row]
        case 3: cell.image.image = BookListeViewController.imagesStyle4[indexPath.row]
        default: break
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                layout collectionViewLayout: UICollectionViewLayout,
                                sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = self.biblioCollectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "biblioHeaderView", for: indexPath) as! BiblioHeaderView
            
            switch indexPath.section {
            case 0: headerView.titre.text = "Collection générée avec style Van Gogh"
            case 1: headerView.titre.text = "Collection générée avec style publicitaire"
            case 2: headerView.titre.text = "Collection générée avec style comics"
            case 3: headerView.titre.text = "Collection générée avec style steampunk"
            default: headerView.titre.text = ""
            }
            
            return headerView
        default: break
        }
        
        return UICollectionReusableView(frame: CGRect.zero)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }

    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 100, height: 40)
    }
    
    @IBAction func clicShare() {
        let images = /*BookListeViewController.imagesStyle1 + BookListeViewController.imagesStyle2 + BookListeViewController.imagesStyle3 +*/ BookListeViewController.imagesStyle4
        
        let objectsToShare = images
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.excludedActivityTypes = [.addToReadingList, .assignToContact, .copyToPasteboard, .mail, .markupAsPDF, .message, .openInIBooks, .saveToCameraRoll, .postToWeibo, .postToWeibo, .postToVimeo, .postToTwitter, .postToFlickr, .postToFacebook, .postToTencentWeibo, .print]
        self.present(activityVC, animated: true, completion: nil)
    }
}
