//
//  ViewController.swift
//  AI24HCODE
//
//  Created by David DRISSI on 01/04/2023.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

    @IBOutlet var previewImageView: UIImageView!
    @IBOutlet var boutonChoixImage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.global().async {

        }
    }
    
    @IBAction func clicChoixImage() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        //self.dismiss(animated: true, completion: { () -> Void in
            //self.previewImageView.image = image
        //})
        
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.originalImage] as? UIImage
        
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
    }
    
    
}

