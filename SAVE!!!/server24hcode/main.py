from flask import Flask, request, jsonify
import datetime
import base64
import torch
from io import BytesIO
from clipseg.models.clipseg import CLIPDensePredT
from PIL import Image
import requests
from torchvision import transforms
from matplotlib import pyplot as plt
from diffusers import StableDiffusionInpaintPipeline
import threading

if torch.backends.mps.is_available():
    mps_device = torch.device("mps")
    x = torch.ones(1, device=mps_device)
    print (x)
else:
    print ("MPS device not found.")

print("CLIP")
# load model
model = CLIPDensePredT(version='ViT-B/16', reduce_dim=64)
model.eval();

print("model.load")
# non-strict, because we only stored decoder weights (not CLIP weights)
model.load_state_dict(torch.load('weights/rd64-uni.pth', map_location=torch.device('cpu')), strict=False);


print("transform")
transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    transforms.Resize((352, 352)),
])

print("pipe")
pipe = StableDiffusionInpaintPipeline.from_pretrained("stabilityai/stable-diffusion-2-inpainting",torch_dtype=torch.float32)

print("Flask")
app = Flask(__name__)

def thread_function(data):

    #print("Thread %s: starting", data)

    image_data = data["image"]
    prompts = data["a_remplacer"]
    remplace = data["remplace"]

    print(len(image_data))
    print(prompts)
    print(len(remplace))

    # # Convertir la chaîne Base64 en octets
    image_bytes = base64.b64decode(image_data)
    im = Image.open(BytesIO(image_bytes))

    img = transform(im).unsqueeze(0)

    # predict
    with torch.no_grad():
        preds = model(img.repeat(4, 1, 1, 1), prompts)[0]

    # visualize prediction
    _, ax = plt.subplots(1, 5, figsize=(15, 4))
    [a.axis('off') for a in ax.flatten()]
    ax[0].imshow(im)
    [ax[i + 1].imshow(torch.sigmoid(preds[i][0])) for i in range(4)];
    [ax[i + 1].text(0, -15, prompts[i]) for i in range(4)];

    for i in range(4):
        # Convertir la sortie en image PIL
        output = torch.sigmoid(preds[i][0]) * 255
        output_np = output.cpu().numpy().astype('uint8')
        mask_image = Image.fromarray(output_np)
        #mask_image.show()


    output = torch.sigmoid(preds[0][0]) * 255
    output_np = output.cpu().numpy().astype('uint8')
    mask_image = Image.fromarray(output_np)
    #mask_image.show()

    mask_image.save("image.jpg", "JPEG")
    
    new_size = mask_image.size
    
    # Redimensionner l'image im1 à la taille de im2
    im_resized = im.resize((512,512))
    mask_image_resized = mask_image.resize((512,512))
    
    # Afficher les images redimensionnées
    #im_resized.show()
    #mask_image_resized.show()

    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    threshold = 50
    pil_image_bw = mask_image_resized.convert('L').point(lambda x: 0 if x < threshold else 255, mode='1')


    prompt = "grass"
    #image and mask_image should be PIL images.
    #The mask structure is white for inpainting and black for keeping as is
    image = pipe(prompt=prompt, image=im_resized, mask_image=pil_image_bw, num_inference_steps=10).images[0]
    image.save("resultat.jpg", "JPEG")
    
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    
    print("Thread %s: finishing", name)


    
@app.route("/image_load", methods=["POST"])
def process_image():
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    data = request.get_json(force=True)
    
    image_data = data["image"]                                                                               #
    prompts = data["a_remplacer"]                                                                            #
    remplace = data["remplace"]                                                                              #
                                                                                                             #
    print(len(image_data))                                                                                   #
    print(prompts)                                                                                           #
    print(len(remplace))                                                                                     #
                                                                                                             #
    # # Convertir la chaîne Base64 en octets                                                                 #
    image_bytes = base64.b64decode(image_data)                                                               #
    im = Image.open(BytesIO(image_bytes))                                                                    #
                                                                                                             #
    img = transform(im).unsqueeze(0)                                                                         #
                                                                                                             #
    # predict                                                                                                #
    with torch.no_grad():                                                                                    #
        preds = model(img.repeat(4, 1, 1, 1), prompts)[0]                                                    #
                                                                                                             #
    # visualize prediction                                                                                   #
    #_, ax = plt.subplots(1, 5, figsize=(15, 4))                                                              #
    #[a.axis('off') for a in ax.flatten()]                                                                    #
    #ax[0].imshow(im)                                                                                         #
    #[ax[i + 1].imshow(torch.sigmoid(preds[i][0])) for i in range(4)];                                        #
    #[ax[i + 1].text(0, -15, prompts[i]) for i in range(4)];                                                  #
                                                                                                             #
    for i in range(4):                                                                                       #
        # Convertir la sortie en image PIL                                                                   #
        output = torch.sigmoid(preds[i][0]) * 255                                                            #
        output_np = output.cpu().numpy().astype('uint8')                                                     #
        mask_image = Image.fromarray(output_np)                                                              #
        #mask_image.show()                                                                                   #
                                                                                                             #
                                                                                                             #
    output = torch.sigmoid(preds[0][0]) * 255                                                                #
    output_np = output.cpu().numpy().astype('uint8')                                                         #
    mask_image = Image.fromarray(output_np)                                                                  #
    #mask_image.show()                                                                                       #
                                                                                                             #
    mask_image.save("image.jpg", "JPEG")                                                                     #
                                                                                                             #
    new_size = mask_image.size                                                                               #
                                                                                                             #
    # Redimensionner l'image im1 à la taille de im2                                                          #
    im_resized = im.resize((512,512))                                                                        #
    mask_image_resized = mask_image.resize((512,512))                                                        #
                                                                                                             #
    # Afficher les images redimensionnées                                                                    #
    #im_resized.show()                                                                                       #
    #mask_image_resized.show()                                                                               #
                                                                                                             #
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))                                             #
    threshold = 50                                                                                           #
    pil_image_bw = mask_image_resized.convert('L').point(lambda x: 0 if x < threshold else 255, mode='1')    #
                                                                                                             #
                                                                                                             #
    prompt = "grass"                                                                                         #
    #image and mask_image should be PIL images.                                                              #
    #The mask structure is white for inpainting and black for keeping as is                                  #
    image = pipe(prompt=prompt, image=im_resized, mask_image=pil_image_bw, num_inference_steps=1).images[0] #
    image.save("resultat.jpg", "JPEG")                                                                       #
                                                                                                             #
    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))                                             #
    
    buffered = BytesIO()
    image.save(buffered, format="JPEG")
    img_str = base64.b64encode(buffered.getvalue())
    return jsonify(image=str(img_str))

    #file = request.files['image']
    # Read the image via file.stream
    #img = Image.open(file.stream)

    #return jsonify({'msg': 'success', 'size': [img.width, img.height]})
    #return jsonify({'msg': 'success', 'size': [10, 10]})

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port="8080")
